int cols, rows;
int scl = 25;
int terrainWidth = 7000;
int terrainHeight = 4200;
int mapTop = 100;
int mapBottom = -125;
int waterLevel = -40;
int hillLevel = -10;

float rollSpeed = 0;

float[][] terrain;

void setup() {
  size(1920, 1080, P3D);
  cols = terrainWidth / scl;
  rows = terrainHeight / scl;
  terrain = new float[cols][rows];
}

void draw() {
  lights();
  background(135,206,235,1);
  noStroke();
  float r = 0;
  float g = 0;
  float b = 0;
  
  rollSpeed -= 0.2;
  float yoff = rollSpeed;
  for (int y = 0; y < rows; y++, yoff += 0.2) {
    float xoff = 0;
    for (int x = 0; x < cols; x++, xoff += 0.2) {
      float z = map(noise(xoff, yoff), 0, 1, mapBottom, mapTop);
      terrain[x][y] = (z <= waterLevel) ? waterLevel : z;
    }
  }
  
  translate(width / 2, 0, -1000);
  rotateX(PI / 4);
  translate(-terrainWidth / 2, -terrainHeight / 2);
  
  for (int y = 0; y < rows-1; y++) {
    beginShape(TRIANGLE_STRIP);
    for (int x = 0; x < cols; x++) {
      vertex(x * scl, y * scl, terrain[x][y]);
      vertex(x * scl, (y + 1) * scl, terrain[x][y+1]);
      if (terrain[x][y] == waterLevel) {
        r = map(terrain[x][y], mapBottom, mapTop, 0, 50);
        g = map(terrain[x][y], mapBottom, mapTop, 50, 150);
        b = map(terrain[x][y], mapBottom, mapTop, 100, 200);
      } else if (terrain[x][y] > waterLevel && terrain[x][y] < hillLevel) {
        r = map(terrain[x][y], mapBottom, mapTop, 75, 125);
        g = map(terrain[x][y], mapBottom, mapTop, 40, 80);
        b = map(terrain[x][y], mapBottom, mapTop, 0, 40);
      } else {
        r = map(terrain[x][y], mapBottom, mapTop, 0, 10);
        g = map(terrain[x][y], mapBottom, mapTop, 80, 120);
        b = map(terrain[x][y], mapBottom, mapTop, 0, 20);
      }
      fill(r,g,b);
    }
    endShape();
  }
}